import { parse, toFountain, estimateLength } from "./mod.ts";

const HELP = `

Commands:
  estimate <source file>
  fountain <source file> <output file>

`.trim();

// by google bard
function formatDuration(seconds: number): string {
  const hours = Math.floor(seconds / (60 * 60));
  const minutes = Math.floor((seconds % (60 * 60)) / 60);
  const secondsLeft = Math.floor(seconds % 60);

  const hoursString = hours > 0 ? `${hours} hour${hours > 1 ? "s" : ""}` : "";
  const minutesString =
    minutes > 0 ? `${minutes} minute${minutes > 1 ? "s" : ""}` : "";
  const secondsString =
    secondsLeft > 0 ? `${secondsLeft} second${secondsLeft > 1 ? "s" : ""}` : "";
  return [hoursString, minutesString, secondsString].filter(Boolean).join(", ");
}

switch (Deno.args[0]) {
  case "estimate": {
    const text = Deno.readTextFileSync(Deno.args[1]);
    const seconds = estimateLength(parse(text));
    console.log(formatDuration(seconds));
    break;
  }
  case "fountain": {
    const text = Deno.readTextFileSync(Deno.args[1]);
    Deno.writeTextFileSync(Deno.args[2], toFountain(parse(text)));
    console.log("Written to " + Deno.args[2]);
    break;
  }
  default: {
    console.log(HELP);
  }
}
