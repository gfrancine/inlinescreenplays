CUT TO:

## Scene - Night
> Blockquotes are synopses.

Headings 2-6 count as scenes.

INT. Scene - Day
> This is another scene.

Alice: (quietly) Dialogue.
Bob: Dialogue.

Alice does something.

Bob (CONT'D): Dialogue.
Alice: Dialogue.
  Bob: Indents turn into double dialogue.

**bold**, *italic*, _underline_.

**INT. Scene - Day**
> This is another scene.

Bob does something.