/*
CORE LIBRARY - features enough to:
- convert to fountain
- estimate time with dialogue and action

Todo:
- iterator
- start and end position
*/

export type Block = {
  type:
    | "text"
    | "dialogue"
    | "parenthetical"
    | "character"
    | "characterDual"
    | "scene"
    | "transition"
    | "synopsis";
  text: string;
};

export function parse(source: string): Block[] {
  let index = 0;
  let blocks: Block[] = [];
  let currentTextBlock: Block = {
    type: "text",
    text: "",
  };

  while (true) {
    let result: null | RegExpMatchArray = null;
    const slice = source.substring(index);
    let matched = false;

    const flush = () => {
      blocks.push(currentTextBlock);
      currentTextBlock = {
        type: "text",
        text: "",
      };
    };

    // synopsis
    result = slice.match(/^> ([^\n]+)/);
    if (result) {
      flush();
      blocks.push({
        type: "synopsis",
        text: result[1],
      });
      index += result[0].length;
      continue;
    }

    // scene

    result = slice.match(/^#{2,6}[^\S\n]+([^\n]+)/);
    if (result) {
      flush();
      blocks.push({
        type: "scene",
        text: result[1],
      });
      index += result[0].length;
      continue;
    }

    result = slice.match(/^\**(INT\.|EXT\.)[^\n]+/); // todo: strip the formatting? does .**[text]** work in fountain?
    if (result) {
      flush();
      blocks.push({
        type: "scene",
        text: result[0],
      });
      index += result[0].length;
      continue;
    }

    // dialogue
    result = slice.match(/^([^\n:]+):[^\S\n]*(\([^\n]+\))?([^\n]+)/);
    if (result) {
      flush();
      // dual dialogue character
      if (result[1].match(/^[^\S\n]+/)) {
        blocks.push({
          type: "characterDual",
          text: result[1].trim(),
        });
      } else {
        blocks.push({
          type: "character",
          text: result[1].trim(),
        });
      }

      //parenthetical

      if (result[2]) {
        blocks.push({
          type: "parenthetical",
          text: result[2].trim(),
        });
      }

      blocks.push({
        type: "dialogue",
        text: result[3].trim(),
      });
      index += result[0].length;
      matched = true;
      continue;
    }

    // transition
    result = slice.match(/^([^\n:]+:)[^\S\n]*/);
    if (result) {
      flush();
      blocks.push({
        type: "transition",
        text: result[1],
      });
      index += result[0].length;
      continue;
    }

    if (index > source.length) {
      flush();
      break;
    }

    // nothing happens
    if (slice[0]) currentTextBlock.text += slice[0];
    index++;
  }

  return blocks;
}

export function toFountain(blocks: Block[]) {
  let fountain = "";

  for (const block of blocks) {
    switch (block.type) {
      case "scene": {
        fountain += "\n." + block.text;
        break;
      }
      case "synopsis": {
        fountain += "\n= " + block.text;
        break;
      }
      case "parenthetical": {
        fountain += "\n" + block.text;
        break;
      }
      case "transition": {
        fountain += "\n> " + block.text;
        break;
      }
      case "characterDual": {
        fountain += "\n@" + block.text + " ^";
        break;
      }
      case "character": {
        fountain += "\n@" + block.text;
        break;
      }
      default: {
        fountain += "\n" + block.text;
        break;
      }
    }
  }

  return fountain.trim();
}
