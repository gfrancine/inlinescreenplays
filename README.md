# inlinescreenplays

## Background

Despite the convenience, I find the standard screenplay format too spacious and
unnatural to write in.

This is a personal and opinionated toolkit I wrote to 1) write scripts the natural
way that's taught back in primary school and port them into the screenplay format, and 2) to save my old scripts. 

It makes use of Markdown for rich text and converts to Fountain, a popular open markup format for screenwriting.

## Reference

```markdown
CUT TO:

## Scene - Night
> Blockquotes are synopses.

Headings 2-6 count as scenes.

INT. Scene - Day
> This is another scene.

Alice: (quietly) Dialogue.
Bob: Dialogue.

Alice does something.

Bob (CONT'D): Dialogue.
Alice: Dialogue.
  Bob: Indents turn into double dialogue.

**bold**, *italic*, _underline_.

**INT. Scene - Day**
> This is another scene.

Bob does something.
```

1. Scenes: Lines that start with INT. or EXT., and optional asterisks (\*) before them (inline formatting).
2. Transitions: Lines with characters followed by ":" (and optionally trailing whitespaces).
3. Dialogue lines: lines with characters followed by ":", and followed by more characters. Supports parentheticals.
4. Dual dialogue: Dialogue lines that start with an indent will form a dual dialogue pair with the one above it.
5. Synopsis or comments: Lines that start with "> ", equivalent to blockquotes in Markdown
6. Everything else will be considered action and subject to the formatting rules in markdown/fountain.


