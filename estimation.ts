import type { Block } from "./core.ts";

export function countWords(text: string): number {
  const matches = text.match(/\b[^\d\W]+\b/g);
  return matches ? matches.length : 0;
}

export function estimateLength(blocks: Block[]) {
  let time = 0; // seconds

  for (const block of blocks) {
    switch (block.type) {
      case "dialogue":
        time += (60 / 150) * countWords(block.text);
      case "text":
        time += (60 / 250) * countWords(block.text);
    }
  }

  return time;
}
